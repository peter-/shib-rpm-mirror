Ansible role "shib-rpm-mirror"
==============================

Configures a server to accept "push"-style uploads using the SFTP protocol from [shibboleth.net](https://shibboleth.net/downloads/service-provider/latest/RPMS/) for package mirroring purposes.

* Uses OpenSSHd's built-in `chroot` functionality to jail the uploading user
* Intended for use with the [lftp](http://lftp.yar.ru/) client using only SFTP as protocol.
* `lftp` can do pretty much everything `rsync` can (even over SFTP) and this method does *not* require a login shell for the uploading user (plus hacks to then try to secure that login shell, e.g. using `rssh` or `rush`).

Requirements
------------

Nothing outside a working Ansible installation on your control machine.

Ansible needs root access on the target machine, as usual. If you're not running Ansible to connect as `root` directly you'll need to add `--become` to your `ansible-playbook` invocation or inventory. You also may need to add the `sudo` package to the target machine.

Web server
----------

This role *optionally* includes a very basic web server (using Nginx) to publish the uploaded files. This is currently only implemented for Debian/Ubuntu systems. It is not enabled by default since you may already be running a web server on the target machine and configuring a full-featured web server goes far beyond what's in scope for this role.

To enable installation and configuration of the web server add `-e 'mirror_nginx=True'` to your invocation of `ansible-playbook` (or to your Ansible inventory or `host_vars`).

Example Playbook
----------------

Example of how to use this role:

```yaml
- hosts: your-mirrors
  roles:
    - peter-.shib-rpm-mirror
```

Or when also including the web server part:

```yaml
- hosts: your-mirrors
  roles:
  - { role: peter-.shib-rpm-mirror, mirror_nginx: True }

```


Additional target machine preparations for some distributions
-------------------------------------------------------------

* Debian 8 "Jessie" without Python installed:  
  Run `apt-get install -y --no-install-recommends python-simplejson libpython2.7-stdlib` on the target (or as argument to `ansible -m raw -a` on your Ansible control machine) once before calling Ansible.
* Ubuntu 16.04 or later without Python 2 installed:  
  Run Ansible with `-e 'ansible_python_interpreter=/usr/bin/python3'` to let Ansible use the existing Python3 install on the target machine. Of course you can set this elsewhere in your `group_vars` or `host_vars` or in your `inventory` or any of the gazillion places Ansible lets you set variables.
* CentOS/RHEL: This role has not been adapted to work with with SELinux enabled, so you'd either have to add support for that yourself (PRs welcome) or disable SELinux before running Ansible with this role.


Role variables
--------------

For mirroring Shibboleth RPM files you should not need to change anything, but you can of course decide to change the name of the connecting user, or the paths where uploaded files are stored:

```yaml
mirror_user_name: mirror
mirror_user_home: /home/mirror
mirror_chroot_dir: /mirror
mirror_chroot_upload_dir: upload
```

These all have specific functions and carefully restricted permissions, so should not be changed without understanding:

* `mirror_user_home` must be read-only to the connecting user, to prevent it from messing with `~/.ssh/authorized_keys`
* `mirror_chroot_dir` must be read-only to the connecting user, as that's a requirement from the OpenSSH chroot.
* `mirror_chroot_upload_dir` must be directly *within* the `mirror_chroot_dir`, and needs to be writable to the connecting user if they're supposed to be able to upload anything. I.e., the default will make `/mirror/upload` the directory where files can be uploaded to, when seen from the OS.  
   (Note that when seen from the jailed user `mirror_chroot_dir` will become the file system `/` directory and so uploads will need to be pushed to `/upload/`.)

> N.B.: You will want to put `mirror_chroot_dir` on another device/disk/volume, to prevent the mirror from filling up your `/` partition, and also to more easily separate VM snapshots or backups of the OS from the mirrored files.

The other variables determine who (what SSH public key) may log in, and from where (what hostname or IP address). The default already includes the [SSH public key published by the Shibboleth developers](https://shibboleth.net/downloads/SSH_KEYS) and restricts use of that key to connections from the IP address of the control host `shibboleth.net`.

For testing purposes (while setting up the machine using this role) you could also add a key and a host pattern of your own, e.g.:

```yaml
mirror_allow_from:
- host_pattern: "3.213.250.186,2600:1f18:3fe:4202:a0a8:a500:a10a:3350"
  key_file: ./files/shibwww.pub
- host_pattern: '192.168.0.*,!192.168.0.9'
  key_file: ~/.ssh/your-test-key.pub
```

Each entry has an OpenSSH `host_pattern` (a string from where to allow the connection) and a `key_file` (referencing the SSH public key that is allowed to connect from matching hosts). See section `PATTERNS` in [`man ssh_config`](https://man.openbsd.org/ssh_config#PATTERNS) for more info on host patterns.

Client side usage with lftp
---------------------------

> Before starting lftp make sure to add any mirror server's SSH host keys to your `~/.ssh/known_hosts` (or `/etc/ssh/ssh_known_hosts`) file, either by using [ssh-keyscan](https://man.openbsd.org/ssh-keyscan) or by manually attempting a connection over SSH once (which should fail but still add the `known_hosts` entry.)

Here's an example of how to push ("reverse mirror") files to a server set up using this role with the lftp client, assuming the default userid (`mirror`) and paths (`/upload` as seen from inside the chroot) from this role, and assuming an SSH private key in `~/.ssh/mirror` exists on the host you're pushing the data from:

Set up lftp once to use a specific SSH private key (be careful not to overwrite your existing lftp client config, if you've used lftp before):

```sh
echo 'set sftp:connect-program "ssh -a -x -i ~/.ssh/mirror"' > ~/.lftprc
```

Then start the sync on one of the machines allowed to connect (using a key allowed to connect) to the machine prepared with this role, recursively syncing all files in your local file system path `/local/source/dir/` to the target directory `/upload/` on the target machine:

```sh
lftp -u mirror,"" -e "mirror --reverse --delete /local/source/dir/ /upload/; quit" sftp://target.example.org
```

License
-------

Apache 2.0
